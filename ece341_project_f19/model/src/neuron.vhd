-- not being used right now ***

use custom_types.all;
library ieee_proposed; 
library ieee;
use ieee_proposed.fixed_pkg.all;  
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

entity neuron is  
	port(
		WEIGHTS : in weights_array(0 to 20); -- WEIGHTS[0] is the bias 
		OUTPUT  : out sfixed(7 downto -8);
		DONE    : out std_logic -- Active high (1) when the calculation is done (OUTPUT is ready)
	);
end entity neuron;