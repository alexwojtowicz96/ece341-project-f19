library model;
use model.custom_types.all;
library ieee;
use ieee.MATH_REAL.all;
use ieee.STD_LOGIC_UNSIGNED.all;

-- Add your library and packages declaration here ...  
use custom_types.all;
library ieee_proposed; 
library ieee;
use ieee_proposed.fixed_pkg.all;  
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

entity dataflow_model_tb is
	-- Generic declarations of the tested unit
	generic(
		number_of_inputs : INTEGER := 10;
		number_of_layers : INTEGER := 2;
		max_num_neurons_in_layer : INTEGER := 20;
		number_of_neurons_per_layer : int_array(0 to 2)  := (10, 10, 5);
		number_of_weights_per_neuron_in_layer : int_array(0 to 1) := (3, 3);
		sfixed_first : INTEGER := 7;
		sfixed_second : INTEGER := -8 
	);
end dataflow_model_tb;

architecture DATAFLOW_TB_ARCHITECTURE of dataflow_model_tb is
	-- Component declaration of the tested unit
	component model
		generic(
			number_of_inputs : INTEGER := 2;
			number_of_layers : INTEGER := 2;
			max_num_neurons_in_layer : INTEGER := 20;
			number_of_neurons_per_layer : int_array(0 to 2);
			number_of_weights_per_neuron_in_layer : int_array(0 to 1);
			sfixed_first : INTEGER := 7;
			sfixed_second : INTEGER := -8 
		);
		port(
			LOAD : in STD_LOGIC;
			INPUT : in inputs_array;
			START : in STD_LOGIC;
			hasStarted : inout STD_LOGIC;
			CLK : in STD_LOGIC;
			LOAD_LAYER : in layer_matrix;
			LOAD_LAYER_NUMBER : in STD_LOGIC_VECTOR(0 to 3);
			LOAD_DONE : out STD_LOGIC;
			DONE : buffer STD_LOGIC;
			OUTPUT_1 : out sfixed(sfixed_first downto sfixed_second);
			OUTPUT_2 : out sfixed(sfixed_first downto sfixed_second);
			OUTPUT_3 : out sfixed(sfixed_first downto sfixed_second);
			OUTPUT_4 : out sfixed(sfixed_first downto sfixed_second);
			OUTPUT_5 : out sfixed(sfixed_first downto sfixed_second) 
		);
	end component;

	-- Stimulus signals - signals mapped to the input and inout ports of tested entity
	signal LOAD : STD_LOGIC;
	signal INPUT : inputs_array;
	signal START : STD_LOGIC;
	signal CLK : STD_LOGIC;
	signal LOAD_LAYER : layer_matrix;
	
	-- Observed signals - signals mapped to the output ports of tested entity
	signal hasStarted : STD_LOGIC;
	signal LOAD_LAYER_NUMBER : STD_LOGIC_VECTOR(0 to 3);
	signal LOAD_DONE : STD_LOGIC;
	signal DONE : STD_LOGIC;
	signal OUTPUT_1 : sfixed(sfixed_first downto sfixed_second);
	signal OUTPUT_2 : sfixed(sfixed_first downto sfixed_second);
	signal OUTPUT_3 : sfixed(sfixed_first downto sfixed_second);
	signal OUTPUT_4 : sfixed(sfixed_first downto sfixed_second);
	signal OUTPUT_5 : sfixed(sfixed_first downto sfixed_second);

	-- add additional signals/variables here ...
	signal clk_half_period : time := 25 ns; -- 200 ns clock period	 

begin
	-- Unit Under Test port map
	UUT : model
		generic map (
			number_of_inputs => number_of_inputs,
			number_of_layers => number_of_layers,
			max_num_neurons_in_layer => max_num_neurons_in_layer,
			number_of_neurons_per_layer => number_of_neurons_per_layer,
			number_of_weights_per_neuron_in_layer => number_of_weights_per_neuron_in_layer,
			sfixed_first => sfixed_first,
			sfixed_second => sfixed_second
		)
		port map (
			LOAD => LOAD,
			INPUT => INPUT,
			START => START,
			hasStarted => hasStarted,
			CLK => CLK,
			LOAD_LAYER => LOAD_LAYER,
			LOAD_LAYER_NUMBER => LOAD_LAYER_NUMBER,
			LOAD_DONE => LOAD_DONE,
			DONE => DONE,
			OUTPUT_1 => OUTPUT_1,
			OUTPUT_2 => OUTPUT_2,
			OUTPUT_3 => OUTPUT_3,
			OUTPUT_4 => OUTPUT_4,
			OUTPUT_5 => OUTPUT_5
		);

	-- Add your stimulus here ...  		
	process
	begin  
		CLK <= '0'; wait for clk_half_period;
		CLK <= '1'; wait for clk_half_period;
	end process;
	
	process
		variable layer_1_data : layer_matrix;
		variable layer_2_data : layer_matrix;
		variable tmp_weights_array_1 : weights_array(0 to max_num_neurons_in_layer);
		variable tmp_weights_array_2 : weights_array(0 to max_num_neurons_in_layer);
		variable empty_weights_array : weights_array(0 to max_num_neurons_in_layer);
	begin						  
		-- initialize the empty weights array
		for i in 0 to max_num_neurons_in_layer loop
			empty_weights_array(i) := to_sfixed(0, sfixed_first, sfixed_second); --"0000000000000000";
		end loop;  
		
		-- develop the first layer
		for i in 0 to max_num_neurons_in_layer-1 loop
			if(i mod 2 = 0) then
				layer_1_data(i)(0) := to_sfixed(2.9531, sfixed_first, sfixed_second); -- loading bias for later 1 neuron 0,2,4... first neuron of every xor
			else
				layer_1_data(i)(0) := to_sfixed(-2.9171, sfixed_first, sfixed_second); -- loading bias for later 1 neuron 1,3,5... second neuron of every xor			
			end if;
			for j in 1 to max_num_neurons_in_layer  loop 
				if (i mod 2 = 0 and i < 10) then   
					if (i + 1 = j ) then
						layer_1_data(i)(j) := to_sfixed(-5.8151, sfixed_first, sfixed_second);
					elsif (i + 2 = j) then
						layer_1_data(i)(j) := to_sfixed(5.9434, sfixed_first, sfixed_second);
					else
						layer_1_data(i)(j) := to_sfixed(0, sfixed_first, sfixed_second);
					end if;				
				elsif (i < 10) then
						if (i = j ) then
							layer_1_data(i)(j) := to_sfixed(-5.5621, sfixed_first, sfixed_second);
						elsif(i + 1 = j) then
							layer_1_data(i)(j) := to_sfixed(5.3736, sfixed_first, sfixed_second);
						else
							layer_1_data(i)(j) := to_sfixed(0, sfixed_first, sfixed_second);
						end if;
				else
					layer_1_data(i)(j) := to_sfixed(0, sfixed_first, sfixed_second);		
				end if;					
			end loop  ;
		end loop;

		-- develop the second layer
		for i in 0 to max_num_neurons_in_layer - 1 loop
			 	layer_2_data(i)(0) := to_sfixed(3.9713, sfixed_first, sfixed_second);
			for j in 1 to max_num_neurons_in_layer loop	
				if(i < 5) then
					if (i * 2 + 1 = j ) then
						layer_2_data(i)(j) := to_sfixed(-8.4911, sfixed_first, sfixed_second); --"0000001111111000";
					elsif ((i + 1) * 2 = j) then
						layer_2_data(i)(j) := to_sfixed(8.9247, sfixed_first, sfixed_second); --"0000100011101100";
					else 
						layer_2_data(i)(j) := to_sfixed(0, sfixed_first, sfixed_second); --"0000000000000000";
					end if;
				else
					layer_2_data(i)(j) := to_sfixed(0, sfixed_first, sfixed_second);
				end if;			
			end loop;
		end loop;
		
		-- we have our layers. Now load them into the network
		-- first, load layer 1
		wait for 90 ns;
		LOAD <= '1';
		LOAD_LAYER <= layer_1_data;
		LOAD_LAYER_NUMBER <= "0000";
		wait for clk_half_period * 2;
		
		-- now load layer 2	
		LOAD_LAYER <= layer_2_data;
		LOAD_LAYER_NUMBER <= "0001";
		wait for clk_half_period * 3;
		
		-- Load all the inputs
		INPUT(0) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(1) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(2) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(3) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(4) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(5) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(6) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(7) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(8) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(9) <= to_sfixed(0, sfixed_first, sfixed_second);
		
		LOAD <= '0';
		START <= '1','0' after  clk_half_period;
		
		wait for (clk_half_period * 2)*2;
		-- Load all the inputs
		INPUT(0) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(1) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(2) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(3) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(4) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(5) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(6) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(7) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(8) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(9) <= to_sfixed(0, sfixed_first, sfixed_second);		

		START <= '1','0' after  clk_half_period;
		
		wait for (clk_half_period * 2)*3;
		-- Load all the inputs
		INPUT(0) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(1) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(2) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(3) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(4) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(5) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(6) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(7) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(8) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(9) <= to_sfixed(0, sfixed_first, sfixed_second);		

		START <= '1','0' after  clk_half_period;
		
		wait for (clk_half_period * 2)*4;
		-- Load all the inputs
		INPUT(0) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(1) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(2) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(3) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(4) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(5) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(6) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(7) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(8) <= to_sfixed(0, sfixed_first, sfixed_second);
		INPUT(9) <= to_sfixed(1, sfixed_first, sfixed_second);

		START <= '1','0' after  clk_half_period;
		
		wait for (clk_half_period * 2)*5;
		-- Load all the inputs
		INPUT(0) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(1) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(2) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(3) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(4) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(5) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(6) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(7) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(8) <= to_sfixed(1, sfixed_first, sfixed_second);
		INPUT(9) <= to_sfixed(1, sfixed_first, sfixed_second);

		START <= '1','0' after  clk_half_period;
		
		wait;
	end process;

end DATAFLOW_TB_ARCHITECTURE;

configuration TESTBENCH_FOR_dataflow_model of dataflow_model_tb is
	for DATAFLOW_TB_ARCHITECTURE
		for UUT : model
			use entity work.model(dataflow);
		end for;
	end for;
end TESTBENCH_FOR_dataflow_model;
