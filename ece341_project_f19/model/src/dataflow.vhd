-- This is the architecture for the dataflow model using the entity "model"	

use custom_types.all;
library ieee_proposed; 
library ieee;
use ieee_proposed.fixed_pkg.all;  
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

architecture dataflow of model is  
	signal neural_network : neural_network(0 to number_of_layers - 1);  -- Holds all layers of neurons (where each neuron has a predetermined number of weights)
	
	signal layer_status : integer := -1;				 -- "iterator" for layers within the neural network
	signal neuron_state : integer := -1;                 -- "iterator" for neuron within a layer
	signal weight_iterator : integer := -1;				 -- "iterator" for weights within neurons
	signal start_neuron_computation : boolean := false;	 -- Used as an indicator for when the neuron output is ready to use
	signal x_value_ready : boolean := false; 			 -- Used as indicator for when the x value is ready for the computation of the sigmoid function
	signal layer_done : boolean := false;		         -- Used as an indicator for when all the neuron outputs in a layer have finished being computed
	signal hasStarted1 : boolean := false; 				 -- Temporary signal used to update hasStarted in the entity
	
	signal D : std_logic;                                 -- Temporary signal to update DONE signal in entity concurrently
	signal O1: sfixed(sfixed_first downto sfixed_second); -- Temporary output signal so that we can update OUTPUT_1 concurrently
	signal O2: sfixed(sfixed_first downto sfixed_second); -- Temporary output signal so that we can update OUTPUT_2 concurrently
	signal O3: sfixed(sfixed_first downto sfixed_second); -- Temporary output signal so that we can update OUTPUT_3 concurrently
	signal O4: sfixed(sfixed_first downto sfixed_second); -- Temporary output signal so that we can update OUTPUT_4 concurrently
	signal O5: sfixed(sfixed_first downto sfixed_second); -- Temporary output signal so that we can update OUTPUT_5 concurrently
	
	-- Some of these may not be used.
	signal x_value : sfixed(sfixed_first downto sfixed_second) := "0000000000000000";        -- used in the summation of weights for X (where X is in activation function)
	signal tmp_weights_array : weights_array(0 to max_num_neurons_in_layer);                 -- holds all the weights (including bias) for a neuron	
	signal tmp_layer_matrix : layer_matrix;                                                  -- holds all the collections of weights in a layer
	signal tmp_real : real;                                                                  -- intermediate value used in the x_value computation in the activation function for each neuron
	signal one_as_sfixed : sfixed(sfixed_first downto sfixed_second) := "0000000100000000";  -- 00000001.00000000	
	signal zero_as_sfixed : sfixed(sfixed_first downto sfixed_second) := "0000000000000000"; -- 00000000.00000000
	signal single_neuron_output : sfixed(sfixed_first downto sfixed_second);                 -- an intermediate value used to update a neuron output in the array
	signal neuron_outputs : inputs_array;                                                    -- storing the outputs (based on weights going into neuron and activation function).
	signal temp_neuron_outputs : inputs_array;                                               -- used to update neuron_outputs between iterations of neuron output calculations	
	
	-- Use this for the lookup table (for the activation function). Values are pre-computed
	type lut is array (-50 to 49) of sfixed(sfixed_first downto sfixed_second);
	signal values : lut := (
		to_sfixed(0.006692850924286273, sfixed_first, sfixed_second), -- [-50]
		to_sfixed(0.007391541344283547, sfixed_first, sfixed_second), -- [-49]
		to_sfixed(0.008162571153161644, sfixed_first, sfixed_second), -- [-48]
		to_sfixed(0.009013298652849767, sfixed_first, sfixed_second), -- [-47]
		to_sfixed(0.009951801866906479, sfixed_first, sfixed_second), -- [-46]
		to_sfixed(0.010986942630595574, sfixed_first, sfixed_second), -- [-45]
		to_sfixed(0.012128434984276895, sfixed_first, sfixed_second), -- [-44]
		to_sfixed(0.013386917827667722, sfixed_first, sfixed_second), -- [-43]
		to_sfixed(0.014774031693276327, sfixed_first, sfixed_second), -- [-42]
		to_sfixed(0.016302499371444564, sfixed_first, sfixed_second), -- [-41]
		to_sfixed(0.017986209962095576, sfixed_first, sfixed_second), -- [-40]
		to_sfixed(0.019840305734081957, sfixed_first, sfixed_second), -- [-39]
		to_sfixed(0.0218812709361354  , sfixed_first, sfixed_second), -- [-38]
		to_sfixed(0.024127021417674657, sfixed_first, sfixed_second), -- [-37]
		to_sfixed(0.02659699357687189 , sfixed_first, sfixed_second), -- [-36]
		to_sfixed(0.029312230751362987, sfixed_first, sfixed_second), -- [-35]
		to_sfixed(0.032295464698457885, sfixed_first, sfixed_second), -- [-34]
		to_sfixed(0.03557118927264431 , sfixed_first, sfixed_second), -- [-33]
		to_sfixed(0.03916572279677334 , sfixed_first, sfixed_second), -- [-32]
		to_sfixed(0.043107254941096025, sfixed_first, sfixed_second), -- [-31]
		to_sfixed(0.047425873177577696, sfixed_first, sfixed_second), -- [-30]
		to_sfixed(0.05215356307842975 , sfixed_first, sfixed_second), -- [-29]
		to_sfixed(0.05732417589888195 , sfixed_first, sfixed_second), -- [-28]
		to_sfixed(0.062973356057011   , sfixed_first, sfixed_second), -- [-27]
		to_sfixed(0.06913842034336273 , sfixed_first, sfixed_second), -- [-26]
		to_sfixed(0.07585818002126098 , sfixed_first, sfixed_second), -- [-25]
		to_sfixed(0.08317269649394145 , sfixed_first, sfixed_second), -- [-24]
		to_sfixed(0.09112296101487696 , sfixed_first, sfixed_second), -- [-23]
		to_sfixed(0.09975048911970787 , sfixed_first, sfixed_second), -- [-22]
		to_sfixed(0.10909682119563766 , sfixed_first, sfixed_second), -- [-21]
		to_sfixed(0.11920292202214441 , sfixed_first, sfixed_second), -- [-20]
		to_sfixed(0.13010847436302697 , sfixed_first, sfixed_second), -- [-19]
		to_sfixed(0.14185106490051927 , sfixed_first, sfixed_second), -- [-18]
		to_sfixed(0.15446526508356867 , sfixed_first, sfixed_second), -- [-17]
		to_sfixed(0.16798161486611204 , sfixed_first, sfixed_second), -- [-16]
		to_sfixed(0.18242552380639554 , sfixed_first, sfixed_second), -- [-15]
		to_sfixed(0.1978161114414602  , sfixed_first, sfixed_second), -- [-14]
		to_sfixed(0.2141650169574861  , sfixed_first, sfixed_second), -- [-13]
		to_sfixed(0.23147521650102987 , sfixed_first, sfixed_second), -- [-12]
		to_sfixed(0.24973989440493277 , sfixed_first, sfixed_second), -- [-11]
		to_sfixed(0.2689414213700482  , sfixed_first, sfixed_second), -- [-10]
		to_sfixed(0.28905049737505184 , sfixed_first, sfixed_second), -- [-9 ]
		to_sfixed(0.31002551887244595 , sfixed_first, sfixed_second), -- [-8 ]
		to_sfixed(0.3318122278318947  , sfixed_first, sfixed_second), -- [-7 ]
		to_sfixed(0.35434369377426767 , sfixed_first, sfixed_second), -- [-6 ]
		to_sfixed(0.3775406687982106  , sfixed_first, sfixed_second), -- [-5 ]
		to_sfixed(0.40131233988761494 , sfixed_first, sfixed_second), -- [-4 ]
		to_sfixed(0.4255574831884094  , sfixed_first, sfixed_second), -- [-3 ]
		to_sfixed(0.45016600268759177 , sfixed_first, sfixed_second), -- [-2 ]
		to_sfixed(0.47502081252113054 , sfixed_first, sfixed_second), -- [-1 ]
		to_sfixed(0.500000000000071   , sfixed_first, sfixed_second), -- [0  ]
		to_sfixed(0.5249791874790112  , sfixed_first, sfixed_second), -- [1  ]
		to_sfixed(0.549833997312549   , sfixed_first, sfixed_second), -- [2  ]
		to_sfixed(0.5744425168117295  , sfixed_first, sfixed_second), -- [3  ]
		to_sfixed(0.5986876601125216  , sfixed_first, sfixed_second), -- [4  ]
		to_sfixed(0.6224593312019231  , sfixed_first, sfixed_second), -- [5  ]
		to_sfixed(0.6456563062258625  , sfixed_first, sfixed_second), -- [6  ]
		to_sfixed(0.6681877721682313  , sfixed_first, sfixed_second), -- [7  ]
		to_sfixed(0.6899744811276757  , sfixed_first, sfixed_second), -- [8  ]
		to_sfixed(0.710949502625065   , sfixed_first, sfixed_second), -- [9  ]
		to_sfixed(0.7310585786300635  , sfixed_first, sfixed_second), -- [10 ]
		to_sfixed(0.7502601055951738  , sfixed_first, sfixed_second), -- [11 ]
		to_sfixed(0.7685247834990713  , sfixed_first, sfixed_second), -- [12 ]
		to_sfixed(0.7858349830426096  , sfixed_first, sfixed_second), -- [13 ]
		to_sfixed(0.80218388855863    , sfixed_first, sfixed_second), -- [14 ]
		to_sfixed(0.8175744761936892  , sfixed_first, sfixed_second), -- [15 ]
		to_sfixed(0.8320183851339673  , sfixed_first, sfixed_second), -- [16 ]
		to_sfixed(0.8455347349165055  , sfixed_first, sfixed_second), -- [17 ]
		to_sfixed(0.8581489350995499  , sfixed_first, sfixed_second), -- [18 ]
		to_sfixed(0.8698915256370374  , sfixed_first, sfixed_second), -- [19 ]
		to_sfixed(0.8807970779779153  , sfixed_first, sfixed_second), -- [20 ]
		to_sfixed(0.8909031788044176  , sfixed_first, sfixed_second), -- [21 ]
		to_sfixed(0.9002495108803431  , sfixed_first, sfixed_second), -- [22 ]
		to_sfixed(0.90887703898517    , sfixed_first, sfixed_second), -- [23 ]
		to_sfixed(0.916827303506102   , sfixed_first, sfixed_second), -- [24 ]
		to_sfixed(0.9241418199787789  , sfixed_first, sfixed_second), -- [25 ]
		to_sfixed(0.9308615796566738  , sfixed_first, sfixed_second), -- [26 ]
		to_sfixed(0.9370266439430226  , sfixed_first, sfixed_second), -- [27 ]
		to_sfixed(0.9426758241011488  , sfixed_first, sfixed_second), -- [28 ]
		to_sfixed(0.9478464369215983  , sfixed_first, sfixed_second), -- [29 ]
		to_sfixed(0.952574126822448   , sfixed_first, sfixed_second), -- [30 ]
		to_sfixed(0.9568927450589275  , sfixed_first, sfixed_second), -- [31 ]
		to_sfixed(0.960834277203248   , sfixed_first, sfixed_second), -- [32 ]
		to_sfixed(0.9644288107273752  , sfixed_first, sfixed_second), -- [33 ]
		to_sfixed(0.9677045353015598  , sfixed_first, sfixed_second), -- [34 ]
		to_sfixed(0.9706877692486532  , sfixed_first, sfixed_second), -- [35 ]
		to_sfixed(0.9734030064231429  , sfixed_first, sfixed_second), -- [36 ]
		to_sfixed(0.9758729785823387  , sfixed_first, sfixed_second), -- [37 ]
		to_sfixed(0.9781187290638766  , sfixed_first, sfixed_second), -- [38 ]
		to_sfixed(0.9801596942659292  , sfixed_first, sfixed_second), -- [39 ]
		to_sfixed(0.9820137900379144  , sfixed_first, sfixed_second), -- [40 ]
		to_sfixed(0.9836975006285644  , sfixed_first, sfixed_second), -- [41 ]
		to_sfixed(0.9852259683067319  , sfixed_first, sfixed_second), -- [42 ]
		to_sfixed(0.9866130821723397  , sfixed_first, sfixed_second), -- [43 ]
		to_sfixed(0.9878715650157298  , sfixed_first, sfixed_second), -- [44 ]
		to_sfixed(0.9890130573694106  , sfixed_first, sfixed_second), -- [45 ]
		to_sfixed(0.9900481981330992  , sfixed_first, sfixed_second), -- [46 ]
		to_sfixed(0.9909867013471554  , sfixed_first, sfixed_second), -- [47 ]
		to_sfixed(0.991837428846843   , sfixed_first, sfixed_second), -- [48 ]
		to_sfixed(0.9926084586557207  , sfixed_first, sfixed_second)  -- [49 ]
	);

begin 
	p1: process(LOAD, START, CLK, D, layer_done)  
	begin 		
		if (CLK'event and CLK = '1') then 
			if (START = '1') then 
				LOAD_DONE <= '0'; 
				if (hasStarted1 = true) then
					report "The inference calculation has already been started!" severity error;
				else 
					report "*** STARTED ***" severity error;
					hasStarted1 <= true;
					layer_status <= 0;
				end if;
			elsif (LOAD = '1') then 								   
				neural_network(to_integer(unsigned(LOAD_LAYER_NUMBER))) <= LOAD_LAYER; 
				report "*** LOADING STARTED ***" severity error; 
				-- This loop is only for debugging...
				for k in 0 to number_of_neurons_per_layer(to_integer(unsigned(LOAD_LAYER_NUMBER)) +1 ) - 1 loop
					for l in 0 to (number_of_neurons_per_layer(to_integer(unsigned(LOAD_LAYER_NUMBER)))) loop
						report "Layer (" & integer'image(to_integer(unsigned(LOAD_LAYER_NUMBER))) & ", " & integer'image(k) & ", " & integer'image(l) & ") : " & real'image(to_real(LOAD_LAYER(k)(l))) severity error;
					end loop;
				end loop;
				report "*** LOADING DONE ***" severity error;
				LOAD_DONE <= '1'; -- Signals that the load has finished
			end if;	 
		elsif (D'event and D = '1') then
			hasStarted1 <= false;
		elsif (layer_done'event and layer_done) then
			layer_status <= layer_status + 1;
		end if;
	end process p1; 
	
	p2: process(layer_status)
	begin
		report "LOAD_LAYER_NUMBER changed!" severity error;
		if (hasStarted1 = true) then 
			report "layer_status >= number_of_layers : " & integer'image(layer_status) & " >= " & integer'image(number_of_layers) severity error;
			if (layer_status = 0) then -- write inputs into outputs 
				D <='0'	;
				neuron_outputs <= INPUT; 
				report "*** Initialized neuron_outputs *** " & integer'image(to_integer(INPUT(0))) & " " & integer'image(to_integer(INPUT(1))) severity error;
			elsif (layer_status >= number_of_layers) then -- output is ready	
				report "*** UPDATING FINAL OUTPUT ***" severity error;
				D <= '1';
				O1 <= temp_neuron_outputs(0);
				O2 <= temp_neuron_outputs(1); 
				O3 <= temp_neuron_outputs(2); 
				O4 <= temp_neuron_outputs(3); 
				O5 <= temp_neuron_outputs(4);		
			else   
				report "*** WRITING neuron_outputs FROM temp_neuron_outputs ***" severity error;
				neuron_outputs <= temp_neuron_outputs;
			end if;
		end if;
	end process p2; 
	
	p3: process(neuron_state, layer_status, x_value_ready)
	begin  
		if (hasStarted1 = true) then
			if (layer_status'event and layer_status < number_of_layers) then
				neuron_state <= 0; 
				layer_done <= false;
			elsif (neuron_state'event) then
				if (neuron_state >= number_of_neurons_per_layer(layer_status + 1)) then -- layer is done  
					report "*** LAYER DONE ***" severity error;
					layer_done <= true;	
				else -- compute the output for the neuron
					start_neuron_computation <= true after 10 ns; -- delay this so that neuron_outputs has time to be updated (to temp_neuron_outputs)	
				end if;	
			elsif (x_value_ready'event and x_value_ready) then
				temp_neuron_outputs(neuron_state) <= values(to_integer(x_value * to_sfixed(10.0, sfixed_first, sfixed_second)));  
				neuron_state <= neuron_state + 1;
				start_neuron_computation <= false;
				report "layer:" & integer'image(layer_status) & " " & integer'image(neuron_state) &" | " & " neurol_output : " & real'image(to_real(values(to_integer(x_value * to_sfixed(10.0, sfixed_first, sfixed_second))))) severity error;
			end if;
		end if;
	end process p3;
	
	p4: process(weight_iterator, start_neuron_computation) 
	begin
		if (start_neuron_computation'event and start_neuron_computation) then
			weight_iterator <= 1; 
			x_value_ready <= false;
			x_value <= neural_network(layer_status)(neuron_state)(0);
		elsif (weight_iterator'event) then 
			if (weight_iterator > (number_of_neurons_per_layer(layer_status))) then	-- we're done 
				report "layer:"& integer'image(layer_status) &" "& integer'image(neuron_state) &" | " & " final x_values : " & real'image(to_real(x_value)) severity error;
				-- Check for x value exceeding range. If it does, rewrite it to be the top or bottom of the range depending on its current value
				if (x_value > to_sfixed(4.9, sfixed_first, sfixed_second)) then
					x_value <= to_sfixed(4.9, sfixed_first, sfixed_second);
				elsif (x_value < to_sfixed(-5, sfixed_first, sfixed_second)) then
					x_value <= to_sfixed(-5, sfixed_first, sfixed_second); 
				end if;
				x_value_ready <= true; -- Signals that the x_value is ready to use
			else -- Keep going. Keep summing the weights multiplied by their respective inputs
				report "layer:"& integer'image(layer_status) & " " & integer'image(neuron_state) & " " & integer'image(weight_iterator) & " | " & " x_values : " & real'image(to_real(x_value)) & " + (" & real'image(to_real(neural_network(layer_status)(neuron_state)(weight_iterator)))& " * " & real'image(to_real(neuron_outputs(weight_iterator - 1))) severity error;
				x_value <= resize(x_value + (neural_network(layer_status)(neuron_state)(weight_iterator) * neuron_outputs(weight_iterator - 1)), sfixed_first, sfixed_second);
				weight_iterator <= weight_iterator + 1;
			end if;
		end if;
	end process p4;	   
	
	hasStarted <= '1' when hasStarted1 else '0'; 
	DONE <= D;
		
	-- Updated outputs concurrently. Depending on how many neurons we have in each layer will determine which outputs gets written
	OUTPUT_1 <= O1 when number_of_neurons_per_layer(number_of_layers) >= 1;
	OUTPUT_2 <= O2 when number_of_neurons_per_layer(number_of_layers) >= 2;
	OUTPUT_3 <= O3 when number_of_neurons_per_layer(number_of_layers) >= 3;
	OUTPUT_4 <= O4 when number_of_neurons_per_layer(number_of_layers) >= 4;
	OUTPUT_5 <= O5 when number_of_neurons_per_layer(number_of_layers) >= 5;
		
end architecture dataflow; 
