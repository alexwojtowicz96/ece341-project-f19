library ieee_proposed; 
use ieee_proposed.fixed_pkg.all;

package custom_types is
	type int_array is array (integer range <>) of integer range 0 to 255;  -- array of variable length (of type integer)
	type inputs_array is array (0 to 19) of sfixed(7 downto -8);           -- needs to account for values in range [-1, 1)
	type weights_array is array (integer range <>) of sfixed(7 downto -8); -- each neuron has an array of weights (variable number)
	type layer_matrix is array (0 to 19) of weights_array(0 to 20);        -- each layer has a collection of neuron weights
	type neural_network is array (integer range <>) of layer_matrix;       -- a neural network has a collection of layers	
end custom_types;		 