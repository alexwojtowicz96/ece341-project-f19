-- This is the entity for our neural network
-- Use this activation function for every neuron in the network:
-- a(x) = 1 / (1 + e^(-X))

-- We made the decision to statically limit the amount of neurons per layer.
-- 20 is the max number of neurons that can be in a layer. 
-- 21 is max_number_of_neurons_per_layer + bias (which is just 20 + 1)	

use custom_types.all;
library ieee_proposed; 
library ieee;
use ieee_proposed.fixed_pkg.all;  
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

entity model is	 
	generic(
		number_of_inputs                      : integer := 2;                   -- known number of meaningful inputs (up to 20)
		number_of_layers                      : integer := 2;  					-- known number of layers (hidden layers + output layer)
		max_num_neurons_in_layer              : integer := 20;					-- statically set to 20 (no layer can have > 20 neurons)
		number_of_neurons_per_layer           : int_array(0 to 2) := (2, 2, 1); -- first index represents the inputs (only 2 layers here)		
		number_of_weights_per_neuron_in_layer : int_array(0 to 1) := (3, 3);    -- hardcoded for now
		sfixed_first                          : integer := 7;                   -- 8 bits left of decimal point
		sfixed_second                         : integer := -8                   -- 8 bits right of decimal point
	);
	port(
		LOAD              : in std_logic;				                   -- active high (sychronous)
		INPUT             : in inputs_array;                               -- each bit is an input value (0 or 1 or X)   
		START             : in std_logic;                                  -- active high (synchronous)
		hasStarted        : inout std_logic;                              -- indicates if the inference calculation has started
		CLK               : in std_logic;	                               -- rising-edge triggered clock 
		LOAD_LAYER        : in layer_matrix;                               -- structure that holds all weights for a layer
		LOAD_LAYER_NUMBER : in std_logic_vector(0 to 3);                   -- each binary value maps to a layer where 0000 is layer 1 (accounts for up to 16 layers)
		LOAD_DONE         : out std_logic;                                 -- active high (1 when load is finished) - synchronized with clock edge 
		DONE              : buffer std_logic;                              -- active high when inference is ready (OUTPUT is meaningful) This is a buffer so we can read it
		OUTPUT_1          : out sfixed(sfixed_first downto sfixed_second); -- The value (inference value). There will be up to 5 outputs in this model.
		OUTPUT_2          : out sfixed(sfixed_first downto sfixed_second); -- output 2
		OUTPUT_3          : out sfixed(sfixed_first downto sfixed_second); -- output 3
		OUTPUT_4          : out sfixed(sfixed_first downto sfixed_second); -- output 4
		OUTPUT_5          : out sfixed(sfixed_first downto sfixed_second)  -- output 5
	);
end entity model;	