-- This lookup table is used for the computation of a(x) = 1 / (1 + e^(-X))
-- Each index of the array models a piece of memory address and based on our value
-- of the input (passed as a binary sfixed type), we will return array(to_integer(x * 10))
	
use custom_types.all;
library ieee_proposed; 
library ieee;
use ieee_proposed.fixed_pkg.all;  
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- The inputs and outputs will use sfixed of range <sfixed_first downto sfixed_second> from model
entity activation_hardware is 
	generic(
		sfixed_first  : integer := 7;
		sfixed_second : integer := -8
	);
	port(
		INPUT : in sfixed(sfixed_first downto sfixed_second);
		OUTPUT : out sfixed(sfixed_first downto sfixed_second)
	);
end entity activation_hardware;	 

-- Lookup Table architecture...	
-- This will use steps of 0.1 between -5 and 5 (100 steps total). 
-- Each value is precomputed and stored in this table. This architecture is modeling a piece 
-- of hardware that holds all of these values.
architecture lut of activation_hardware is 
	type lut is array (-50 to 49) of sfixed(sfixed_first downto sfixed_second);
	signal values : lut := (
		to_sfixed(0.006692850924284855, sfixed_first, sfixed_second), --[-50]
		to_sfixed(0.007391541344281971, sfixed_first, sfixed_second), --[-49]
		to_sfixed(0.00816257115315989 , sfixed_first, sfixed_second), --[-48]
		to_sfixed(0.009013298652847815, sfixed_first, sfixed_second), --[-47]
		to_sfixed(0.009951801866904308, sfixed_first, sfixed_second), --[-46]
		to_sfixed(0.010986942630593162, sfixed_first, sfixed_second), --[-45]
		to_sfixed(0.012128434984274213, sfixed_first, sfixed_second), --[-44]
		to_sfixed(0.013386917827664744, sfixed_first, sfixed_second), --[-43]
		to_sfixed(0.014774031693273017, sfixed_first, sfixed_second), --[-42]
		to_sfixed(0.01630249937144089 , sfixed_first, sfixed_second), --[-41]
		to_sfixed(0.017986209962091496, sfixed_first, sfixed_second), --[-40]
		to_sfixed(0.01984030573407743 , sfixed_first, sfixed_second), --[-39]
		to_sfixed(0.021881270936130383, sfixed_first, sfixed_second), --[-38]
		to_sfixed(0.024127021417669092, sfixed_first, sfixed_second), --[-37]
		to_sfixed(0.026596993576865725, sfixed_first, sfixed_second), --[-36]
		to_sfixed(0.02931223075135617 , sfixed_first, sfixed_second), --[-35]
		to_sfixed(0.03229546469845033 , sfixed_first, sfixed_second), --[-34]
		to_sfixed(0.035571189272635965, sfixed_first, sfixed_second), --[-33]
		to_sfixed(0.03916572279676412 , sfixed_first, sfixed_second), --[-32]
		to_sfixed(0.043107254941085846, sfixed_first, sfixed_second), --[-31]
		to_sfixed(0.04742587317756646 , sfixed_first, sfixed_second), --[-30]
		to_sfixed(0.05215356307841737 , sfixed_first, sfixed_second), --[-29]
		to_sfixed(0.05732417589886832 , sfixed_first, sfixed_second), --[-28]
		to_sfixed(0.06297335605699601 , sfixed_first, sfixed_second), --[-27]
		to_sfixed(0.06913842034334627 , sfixed_first, sfixed_second), --[-26]
		to_sfixed(0.07585818002124294 , sfixed_first, sfixed_second), --[-25]
		to_sfixed(0.08317269649392166 , sfixed_first, sfixed_second), --[-24]
		to_sfixed(0.09112296101485534 , sfixed_first, sfixed_second), --[-23]
		to_sfixed(0.09975048911968425 , sfixed_first, sfixed_second), --[-22]
		to_sfixed(0.10909682119561194 , sfixed_first, sfixed_second), --[-21]
		to_sfixed(0.11920292202211644 , sfixed_first, sfixed_second), --[-20]
		to_sfixed(0.1301084743629966  , sfixed_first, sfixed_second), --[-19]
		to_sfixed(0.1418510649004864  , sfixed_first, sfixed_second), --[-18]
		to_sfixed(0.15446526508353317 , sfixed_first, sfixed_second), --[-17]
		to_sfixed(0.16798161486607383 , sfixed_first, sfixed_second), --[-16]
		to_sfixed(0.1824255238063545  , sfixed_first, sfixed_second), --[-15]
		to_sfixed(0.19781611144141623 , sfixed_first, sfixed_second), --[-14]
		to_sfixed(0.21416501695743917 , sfixed_first, sfixed_second), --[-13]
		to_sfixed(0.23147521650098    , sfixed_first, sfixed_second), --[-12]
		to_sfixed(0.24973989440487981 , sfixed_first, sfixed_second), --[-11]
		to_sfixed(0.26894142136999233 , sfixed_first, sfixed_second), --[-10]
		to_sfixed(0.28905049737499305 , sfixed_first, sfixed_second), --[-9 ]
		to_sfixed(0.3100255188723844  , sfixed_first, sfixed_second), --[-8 ]
		to_sfixed(0.3318122278318305  , sfixed_first, sfixed_second), --[-7 ]
		to_sfixed(0.35434369377420094 , sfixed_first, sfixed_second), --[-6 ]
		to_sfixed(0.3775406687981417  , sfixed_first, sfixed_second), --[-5 ]
		to_sfixed(0.40131233988754406 , sfixed_first, sfixed_second), --[-4 ]
		to_sfixed(0.4255574831883369  , sfixed_first, sfixed_second), --[-3 ]
		to_sfixed(0.45016600268751783 , sfixed_first, sfixed_second), --[-2 ]
		to_sfixed(0.47502081252105566 , sfixed_first, sfixed_second), --[-1 ]
		to_sfixed(0.49999999999999556 , sfixed_first, sfixed_second), --[0  ]
		to_sfixed(0.5249791874789355  , sfixed_first, sfixed_second), --[1  ]
		to_sfixed(0.5498339973124733  , sfixed_first, sfixed_second), --[2  ]
		to_sfixed(0.5744425168116544  , sfixed_first, sfixed_second), --[3  ]
		to_sfixed(0.5986876601124473  , sfixed_first, sfixed_second), --[4  ]
		to_sfixed(0.62245933120185    , sfixed_first, sfixed_second), --[5  ]
		to_sfixed(0.6456563062257908  , sfixed_first, sfixed_second), --[6  ]
		to_sfixed(0.6681877721681616  , sfixed_first, sfixed_second), --[7  ]
		to_sfixed(0.6899744811276081  , sfixed_first, sfixed_second), --[8  ]
		to_sfixed(0.7109495026249997  , sfixed_first, sfixed_second), --[9  ]
		to_sfixed(0.7310585786300007  , sfixed_first, sfixed_second), --[10 ]
		to_sfixed(0.7502601055951135  , sfixed_first, sfixed_second), --[11 ]
		to_sfixed(0.7685247834990137  , sfixed_first, sfixed_second), --[12 ]
		to_sfixed(0.7858349830425548  , sfixed_first, sfixed_second), --[13 ]
		to_sfixed(0.8021838885585781  , sfixed_first, sfixed_second), --[14 ]
		to_sfixed(0.8175744761936402  , sfixed_first, sfixed_second), --[15 ]
		to_sfixed(0.8320183851339212  , sfixed_first, sfixed_second), --[16 ]
		to_sfixed(0.8455347349164622  , sfixed_first, sfixed_second), --[17 ]
		to_sfixed(0.8581489350995093  , sfixed_first, sfixed_second), --[18 ]
		to_sfixed(0.8698915256369995  , sfixed_first, sfixed_second), --[19 ]
		to_sfixed(0.8807970779778798  , sfixed_first, sfixed_second), --[20 ]
		to_sfixed(0.8909031788043846  , sfixed_first, sfixed_second), --[21 ]
		to_sfixed(0.9002495108803125  , sfixed_first, sfixed_second), --[22 ]
		to_sfixed(0.9088770389851418  , sfixed_first, sfixed_second), --[23 ]
		to_sfixed(0.9168273035060757  , sfixed_first, sfixed_second), --[24 ]
		to_sfixed(0.9241418199787546  , sfixed_first, sfixed_second), --[25 ]
		to_sfixed(0.9308615796566515  , sfixed_first, sfixed_second), --[26 ]
		to_sfixed(0.937026643943002   , sfixed_first, sfixed_second), --[27 ]
		to_sfixed(0.9426758241011297  , sfixed_first, sfixed_second), --[28 ]
		to_sfixed(0.947846436921581   , sfixed_first, sfixed_second), --[29 ]
		to_sfixed(0.9525741268224319  , sfixed_first, sfixed_second), --[30 ]
		to_sfixed(0.9568927450589126  , sfixed_first, sfixed_second), --[31 ]
		to_sfixed(0.9608342772032344  , sfixed_first, sfixed_second), --[32 ]
		to_sfixed(0.9644288107273629  , sfixed_first, sfixed_second), --[33 ]
		to_sfixed(0.9677045353015485  , sfixed_first, sfixed_second), --[34 ]
		to_sfixed(0.9706877692486428  , sfixed_first, sfixed_second), --[35 ]
		to_sfixed(0.9734030064231335  , sfixed_first, sfixed_second), --[36 ]
		to_sfixed(0.97587297858233    , sfixed_first, sfixed_second), --[37 ]
		to_sfixed(0.9781187290638689  , sfixed_first, sfixed_second), --[38 ]
		to_sfixed(0.9801596942659219  , sfixed_first, sfixed_second), --[39 ]
		to_sfixed(0.982013790037908   , sfixed_first, sfixed_second), --[40 ]
		to_sfixed(0.9836975006285584  , sfixed_first, sfixed_second), --[41 ]
		to_sfixed(0.9852259683067265  , sfixed_first, sfixed_second), --[42 ]
		to_sfixed(0.9866130821723347  , sfixed_first, sfixed_second), --[43 ]
		to_sfixed(0.9878715650157253  , sfixed_first, sfixed_second), --[44 ]
		to_sfixed(0.9890130573694065  , sfixed_first, sfixed_second), --[45 ]
		to_sfixed(0.9900481981330953  , sfixed_first, sfixed_second), --[46 ]
		to_sfixed(0.9909867013471519  , sfixed_first, sfixed_second), --[47 ]
		to_sfixed(0.9918374288468399  , sfixed_first, sfixed_second), --[48 ]
		to_sfixed(0.9926084586557179  , sfixed_first, sfixed_second)  --[49 ]
	);
begin
	process(INPUT)  
	begin
		OUTPUT <= values(to_integer(INPUT * to_sfixed(10.0, sfixed_first, sfixed_second)));
	end process;
end architecture lut;