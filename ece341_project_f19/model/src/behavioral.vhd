-- This is the architecture for the dataflow model using the entity "model"
	
use custom_types.all;
library ieee_proposed; 
library ieee;
use ieee_proposed.fixed_pkg.all;  
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

architecture behavioral of model is		
	signal neural_network : neural_network(0 to number_of_layers - 1);
begin
	process	-- TODO: this doesn't work and idk why
	begin
		hasStarted <= '0'; -- initialize hasStarted to '0'
		wait;
	end process;
	
	process(LOAD, START, CLK)
		-- VARIABLES
		variable layer_index : integer;	                                                           -- used in conversion from std_logic_vector to integer (for LOAD_LAYER_NUMBER)
		variable x_value : sfixed(sfixed_first downto sfixed_second) := "0000000000000000";        -- used in the summation of weights for X (where X is in activation function)
		variable tmp_weights_array : weights_array(0 to max_num_neurons_in_layer);                 -- holds all the weights (including bias) for a neuron	
		variable tmp_layer_matrix : layer_matrix;                                                  -- holds all the collections of weights in a layer
		variable tmp_real : real;                                                                  -- intermediate value used in the x_value computation in the activation function for each neuron
		variable one_as_sfixed : sfixed(sfixed_first downto sfixed_second) := "0000000100000000";  -- 00000001.00000000	
		variable zero_as_sfixed : sfixed(sfixed_first downto sfixed_second) := "0000000000000000"; -- 00000000.00000000
		variable single_neuron_output : sfixed(sfixed_first downto sfixed_second);                 -- an intermediate value used to update a neuron output in the array
		variable neuron_outputs : weights_array(0 to max_num_neurons_in_layer - 1);                -- storing the outputs (based on weights going into neuron and activation function).
		variable temp_neuron_outputs : weights_array(0 to max_num_neurons_in_layer - 1);           -- used to update neuron_outputs between iterations of neuron output calculations
		
	begin 		
		if (CLK'event and CLK = '1') then 
			if (START = '1') then -- start has priority over load 
				LOAD_DONE <= '0';
				DONE <= '0'; -- we want to start a new inference. Reset DONE.
				if (hasStarted = '1') then -- already started, do nothing
					report "The inference calculation has already been started!" severity error;
				else -- has not started and we want to start. start it and do the calculations
					hasStarted <= '1';
		  			for i in 0 to number_of_layers - 1 loop -- loop through every layer in the network
						tmp_layer_matrix := neural_network(i); 
						
						if (i = 0) then
							for x in 0 to max_num_neurons_in_layer - 1 loop
								neuron_outputs(x) := INPUT(x);
								--report "The INPUT AT " & integer'image(x) & " : " & real'image(to_real(INPUT(x))) severity error;
							end loop;
						end if;
						--for x in 0 to max_num_neurons_in_layer - 1 loop
						--	report "LAYER " & integer'image(i) & " - neuron_outputs(" & integer'image(x) & ") : " & real'image(to_real(neuron_outputs(x))) severity error;
						--end loop;
						
						for j in 0 to max_num_neurons_in_layer - 1 loop -- loop through every neuron's collection of weights. 20 is the maximum number of neurons per layer
							tmp_weights_array := tmp_layer_matrix(j);
							x_value := zero_as_sfixed; 
							
							-- Use this to debug if needed
							--report "===== Neuron " & integer'image(j) & " in Layer " & integer'image(i) & " *** WEIGHTS WITH BIAS AT [0] =====" severity error;
							--for w in 0 to max_num_neurons_in_layer loop
							--	 report "tmp_weights_array[" & integer'image(w) & "] : " & real'image(to_real(tmp_weights_array(w))) severity error;
							--end loop;
								
							for k in 0 to max_num_neurons_in_layer - 1 loop -- loop through every weight in this neuron's collection of weights (excluding bias)
								x_value := to_sfixed(to_real(x_value) + to_real((tmp_weights_array(k + 1) * neuron_outputs(k))), sfixed_first, sfixed_second);
							end loop;
							x_value := to_sfixed(to_real(x_value + tmp_weights_array(0)), sfixed_first, sfixed_second); -- add the bias
							--report "with bias => x_value : " & real'image(to_real(x_value)) severity error;
							
							-- compute activation function for neural_network(i)(j) with x_value
							if (j < number_of_neurons_per_layer(i + 1)) then
								tmp_real := (to_real(one_as_sfixed) / (to_real(one_as_sfixed) + exp(to_real(zero_as_sfixed - x_value))));
								single_neuron_output := to_sfixed(tmp_real, sfixed_first, sfixed_second);
							else
								single_neuron_output := zero_as_sfixed;
							end if;
							temp_neuron_outputs(j) := single_neuron_output;
						end loop;										   
						
						neuron_outputs := temp_neuron_outputs; -- once it is done iterating through every layer, update neuron_outputs..
						-- Why? because we are iterating through every neuron in the layer. If we update neuron_outputs within that loop,
						-- our data will be changed every iteration, and we want to keep it constant until we have completed iterating.
					end loop;
					
					-- There are up to 5 outputs. If an output is 0, then it is considered to be unmeaningful
					-- The number of meaningful outputs depends on the number of neurons in the last layer
					-- If there are more than 5 neurons in the last layer, then only the first 5 will have outputs according to this design.
					OUTPUT_1 <= neuron_outputs(0);
					OUTPUT_2 <= neuron_outputs(1);
					OUTPUT_3 <= neuron_outputs(2);
					OUTPUT_4 <= neuron_outputs(3);
					OUTPUT_5 <= neuron_outputs(4);
					report "===== START OF A NEW INFERENCE =====" severity error;
					report "OUTPUT_1 : " & real'image(to_real(neuron_outputs(0))) severity error;
					report "OUTPUT_2 : " & real'image(to_real(neuron_outputs(1))) severity error;
					report "OUTPUT_3 : " & real'image(to_real(neuron_outputs(2))) severity error;
					report "OUTPUT_4 : " & real'image(to_real(neuron_outputs(3))) severity error;
					report "OUTPUT_5 : " & real'image(to_real(neuron_outputs(4))) severity error;
					DONE <= '1';
					
				end if;
			elsif (LOAD = '1') then -- we want to load a layer of weights into the network
				layer_index := to_integer(unsigned(LOAD_LAYER_NUMBER));
				neural_network(layer_index) <= LOAD_LAYER;
				LOAD_DONE <= '1';
				report "Loaded data into layer " & integer'image(layer_index + 1) & " of " & integer'image(number_of_layers) severity error;
			end if;
		end if;
	end process; 
	
	process(DONE)
	begin 
		if (DONE = '1') then -- finished making the inference
			hasStarted <= '0';
		end if;
	end process;
	
end architecture behavioral;